import git
import os
import yaml
import logging
import requests
import json
import base64
import sys

def build_containers(metas):
    requests.packages.urllib3.disable_warnings()
    # bind
    url=cfg['bioshadock']['url']
    data=json.dumps({'uid': cfg['bioshadock']['login'], 'password': cfg['bioshadock']['password']})
    res = requests.post(url+'/user/bind', data, headers={'Content-type': 'application/json', 'Accept': 'application/json'}, verify=False)
    if res.status_code != 200:
        logging.error('Authentication error, quit!')
        return
    token = res.json()['token'].encode('ascii','ignore').decode('utf-8')
    for meta in metas:
        meta_data = None
        if not os.path.exists(meta.replace('docker.new', 'package.yaml')):
            logging.error('package.yaml does not exists: '+meta)
            continue
        with open(meta.replace('docker.new', 'package.yaml'), 'r') as stream:
            try:
                meta_data = yaml.load(stream)
            except Exception as e:
                logging.error("Failed to decode yaml "+meta)
        if meta_data is None:
            logging.warn('Skipping '+meta)
            continue
        meta_data['package']['container'] = meta_data['package']['container'].replace('+','plus')
        meta_data['package']['name'] = meta_data['package']['name'].replace('+','plus')
        container_name = meta_data['package']['container']
        if len(container_name.split(' ')) > 1:
            container_name = container_name.split(' ')[0]
        if meta_data['package']['name'] in cfg['blacklist']:
            logging.warn('Skipping '+ meta_data['package']['name'])
            continue
        logging.warn('Submit '+meta_data['package']['name'])
        res = requests.get(url+'/container/'+container_name, headers={'Content-type': 'application/json', 'Accept': 'application/json'}, verify=False)
        if res.status_code == 200:
            # update
            container = res.json()
            logging.debug("Update container " + container['id'])
            headers = {'Authorization':'Bearer '+token, 'Content-type': 'application/json', 'Accept':'application/json'}
            dockerfilecontent = None
            meta_name = meta_data['package']['name']
            #docker_dir = os.path.join(cfg['docker_dir'], 'bioconda', meta_name)
            dockerfile = meta.replace('docker.new', 'Dockerfile')
            if not os.path.exists(dockerfile):
                logging.error('No Dockerfile for '+container_name)
                continue
            with open(dockerfile, 'r') as stream:
                dockerfilecontent = stream.read()

            res = requests.post(url+'/container/dockerfile/'+container_name, json.dumps({'dockerfile': dockerfilecontent}), headers=headers, verify=False)
            if 'package' in meta_data and 'version' in meta_data['package'] and meta_data['package']['version']:
                package_version = meta_data['package']['version']
                #/container/tag/x/y/z/:tagid
                logging.info("Tag "+container_name+": "+package_version)
                res = requests.get(url+'/container/tag/'+container_name+'/'+package_version, headers=headers, verify=False)

            if os.path.exists(meta):
                os.remove(meta)
            continue

        if res.status_code == 404:
            # create
            logging.debug("Create container "+container_name)
            headers = {'Authorization':'Bearer '+token, 'Content-type': 'application/json', 'Accept':'application/json'}
            meta_name = meta_data['package']['name']
            summary = meta_name
            if 'about' in meta_data and 'summary' in meta_data['about']:
                summary = meta_data['about']['summary']
            dockerfilecontent = None
            #dockerfile = meta
            dockerfile = meta.replace('docker.new', 'Dockerfile')
            if not os.path.exists(dockerfile):
                logging.error('No Dockerfile for '+container_name)
                continue
            with open(dockerfile, 'r') as stream:
                dockerfilecontent = stream.read()
            data = {
                'name': container_name,
                'description': summary,
                'dockerfile': dockerfilecontent,
                'visible': 1
            }
            res = requests.post(url+'/container/new', json.dumps(data), headers=headers, verify=False)
            os.remove(meta)
            continue
        logging.error("Error with container "+container_name)
        continue

cfg = None

with open("config.yml", 'r') as stream:
    try:
        cfg = yaml.load(stream)
    except yaml.YAMLError as exc:
        logging.error(exc)

if len(sys.argv) == 2:
    cont_path = os.path.join(cfg['docker_dir'], sys.argv[1])
    if not cont_path:
        logging.error(sys.argv[1]+' does not exists')
        sys.exit(1)
    cont = os.path.join(cont_path, 'docker.new')
    build_containers([cont])
    sys.exit(0)


metas = []
for root, dirs, filenames in os.walk(os.path.join(cfg['docker_dir'])):
    for f in filenames:
        if f == 'docker.new':
            logging.debug('Meta: '+os.path.join(root, f))
            metas.append(os.path.join(root, f))

build_containers(metas)
